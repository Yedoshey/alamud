# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3


class GiveEvent(Event2):
    NAME = "give"

    def perform(self):
        if not self.object.has_prop("givable"):
            self.add_prop("object-not-givable")
        self.inform("give")

    def give_failed(self):
        self.fail()
        self.inform("give.failed")

class GiveToEvent(Event3):
    NAME = "give-to"

    def perform(self):
        if not self.object.has_prop("givable"):
            self.add_prop("object-not-givable")
            return self.give_failure()
        self.object.move_to(None)
        self.inform("open-with")

    def give_failure(self):
        self.fail()
        self.inform("give.failed")
